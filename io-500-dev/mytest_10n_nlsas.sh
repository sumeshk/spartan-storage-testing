#!/bin/bash

#SBATCH -p debug
#SBATCH -w spartan-gpgpu[001-002,013-014,024-025,035-036,047-048]
#SBATCH --ntasks=10
#SBATCH --tasks-per-node=1
#SBATCH --mem=100G
#SBATCH --cpus-per-task=12
#SBATCH --time=06:00:00

module load OpenMPI/3.1.3-GCC-6.2.0-ucx

./io500_10n_nlsas.sh
