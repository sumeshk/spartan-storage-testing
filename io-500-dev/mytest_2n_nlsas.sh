#!/bin/bash

#SBATCH -p debug
#SBATCH -w spartan-gpgpu[001,030]
#SBATCH --ntasks=2
#SBATCH --tasks-per-node=1
#SBATCH --mem=100G
#SBATCH --cpus-per-task=12
#SBATCH --time=06:00:00

module load OpenMPI/3.1.3-GCC-6.2.0-ucx

./io500_2n_nlsas.sh
